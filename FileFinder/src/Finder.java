import java.io.File;
import java.util.Scanner;
public class Finder {

public static void main(String[] args) {
      Find fileSearch = new Find();
      Scanner scan = new Scanner(System.in);
      System.out.println("Please, write the name of file you are searching (or substring of fileName): ");
      String name = scan.nextLine();
      System.out.println("Do you know the directory of that file? Write the directory name, otherwise write NO");
      String dir = scan.nextLine();
      //if the input is not equal to "NO"
      System.out.println("Searching...");
      if(!dir.equals("NO")) {
    	  //start searching in input directory
    	  fileSearch.search(name, new File(dir));
      }
      else {
    	  //otherwise we search in the entire disk "C"
    	  fileSearch.search(name, new File("C:\\"));
      }
      //fileSearch.getResult().size() is the size of results list
      if(fileSearch.getResult().size() == 0) {
    	  //if the size of list is equal to zero, then there is no result
    	  System.out.println("\nNo result found!");
      }
      else {
    	  //Otherwise we found
    	  //(Here is the list of all results
    	  System.out.println("\n Found " + fileSearch.getResult().size() + " result!\n");
          for (String matched : fileSearch.getResult()) {
              System.out.println("Found : " + matched);
          }
      }
      scan.close();
  }
}
