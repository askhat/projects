import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public  class Find {
	private List<String> result = new ArrayList<String>();
	public List<String> getResult() {
        return result;
    }
    public void search(String name, File file) {
  	  File[] list = file.listFiles();
        if(list != null && file.isDirectory()) {
            //System.out.println("Searching directory ... " + file.getAbsoluteFile());
            if(file.canRead()) {
                for (File subdir : list) {
                    if (subdir.isDirectory()) {
                        search(name, subdir);
                    }
                    else if (name.equals(subdir.getName())) {
                         result.add(subdir.getParent());
                         System.out.println(subdir.getParent());
                    }
                    else {
                        if(Pattern.compile(Pattern.quote(name), Pattern.CASE_INSENSITIVE).matcher(subdir.getName()).find()) {
                        	result.add(subdir.getParent());
                            System.out.println(subdir.getParent());
                        }
                    }
                }
            }
            else {
                System.out.println("Permission denied for " + file.getAbsoluteFile() + "!");
            }

        }
    }
}
